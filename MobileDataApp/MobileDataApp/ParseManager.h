//
//  ParseManager.h
//  MobileDataApp
//
//  Created by Romaisa Riaz on 18/08/2019.
//  Copyright © 2019 Romaisa Riaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSVParser.h"

@interface ParseManager : NSObject

@property (nonatomic, strong) CSVParser *parser;
@property (nonatomic, strong) NSArray *fields;
@property (nonatomic, strong) NSArray *result;

+ (id)sharedManager;

@end
