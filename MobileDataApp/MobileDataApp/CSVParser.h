//
//  CSVParser.h
//  MobileDataApp
//
//  Created by Romaisa Riaz on 17/08/2019.
//  Copyright © 2019 Romaisa Riaz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserData.h"

@interface CSVParser : NSObject

@property (nonatomic, strong) NSString *sourceFileString;
@property (nonatomic, strong) NSMutableArray *csvArray;

- (id) initCSVParser:(NSString *) path;
- (NSMutableArray *)readField;
- (NSMutableArray *)readResult;
- (NSMutableArray *)readResultQaurterFrom: (int)from andTo:(int) to;
- (NSMutableArray*) getConsumptionResultPerYear:(NSMutableArray*) dataArray;


- (int) getYearFromQuarter:(NSString *) valueString;
- (UserData *)getUserDataFromYear:(int)year andVol:(double)volume;

@end
