//
//  DataUsageViewController.h
//  MobileDataApp
//
//  Created by Romaisa Riaz on 18/08/2019.
//  Copyright © 2019 Romaisa Riaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DataUsageViewController : UITableViewController

@property (nonatomic, strong) NSMutableArray *dataUsageResult;

@end
