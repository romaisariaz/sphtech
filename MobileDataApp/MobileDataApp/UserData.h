//
//  UserData.h
//  MobileDataApp
//
//  Created by Romaisa Riaz on 18/08/2019.
//  Copyright © 2019 Romaisa Riaz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserData : NSObject

@property (nonatomic, copy) NSString *year;
@property (nonatomic, copy) NSString *volume;

@end
