//
//  AppDelegate.h
//  MobileDataApp
//
//  Created by Romaisa Riaz on 17/08/2019.
//  Copyright © 2019 Romaisa Riaz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) UIViewController *rootController;


@end

