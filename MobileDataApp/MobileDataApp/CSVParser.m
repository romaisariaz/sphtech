//
//  CVSParser.m
//  MobileDataApp
//
//  Created by Romaisa Riaz on 17/08/2019.
//  Copyright © 2019 Romaisa Riaz. All rights reserved.
//

#import "CSVParser.h"
#import "UserData.h"

@implementation CSVParser
@synthesize sourceFileString;
@synthesize csvArray;

- (id) initCSVParser:(NSString *)path
{
    self = [super init];
    if(self)
    {
        sourceFileString = [NSString stringWithContentsOfFile:[[NSBundle mainBundle] pathForResource:path ofType:@"csv"] encoding:NSUTF8StringEncoding error:nil];
        csvArray = [[NSMutableArray alloc] init];
        csvArray = [[sourceFileString componentsSeparatedByString:@"\n"] mutableCopy];
        
    }
    return self;
}
- (NSMutableArray *) readField
{
    if(!csvArray) {
        NSLog(@"CSV not initialized");
        return nil;
        
    }
    NSString *keysString = [csvArray objectAtIndex:0];
    NSArray *keysArray = [keysString componentsSeparatedByString:@","];
    
    NSMutableArray *fieldArray = [[NSMutableArray alloc] initWithObjects:@{@"text": keysArray[0]},
                                                                        @{@"numeric": keysArray[1]},
                                                                          nil];
    return  fieldArray;
}
- (NSMutableArray *) readResult
{
    NSMutableArray *valuesArray = [[NSMutableArray alloc] init];
    if(!csvArray)
    {
        NSLog(@"CSV not initialized");
        return nil;
    }
    else {
        
        NSInteger count = [csvArray count];
        for( int i = 1; i < count-1; i++)
        {
            NSString *tempString = [csvArray objectAtIndex:i];
            NSArray *dataArray = [tempString componentsSeparatedByString:@","];
            
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            [tempDictionary setObject:dataArray[0] forKey:@"quarter"];
            [tempDictionary setObject:dataArray[1] forKey:@"volume_of_mobile_data"];
            
            [valuesArray addObject:tempDictionary];
        }
        return valuesArray;

    }
}

- (NSMutableArray *)readResultQaurterFrom: (int)from andTo:(int) to
{
    if(from == 0 || to == 0)
    {
        NSLog(@"Incorrect Data Provided");
        return nil;
    }
    
    NSMutableArray *valuesArray = [[NSMutableArray alloc] init];
    if(!csvArray)
    {
        NSLog(@"CSV not initialized");
        return nil;
    }
    else {
        
        NSInteger count = [csvArray count];
        for( int i = 1; i < count-1; i++)
        {
            NSString *tempString = [csvArray objectAtIndex:i];
            NSArray *dataArray = [tempString componentsSeparatedByString:@","];
            
            NSMutableDictionary *tempDictionary = [[NSMutableDictionary alloc] init];
            int year = [self getYearFromQuarter:dataArray[0]];
            if(year >= from && year <= to)
            {
                [tempDictionary setObject:dataArray[0] forKey:@"quarter"];
                [tempDictionary setObject:dataArray[1] forKey:@"volume_of_mobile_data"];
                [valuesArray addObject:tempDictionary];
            }
        }
        NSLog(@"%@", valuesArray);
        return valuesArray;
        
    }
    
}

- (NSMutableArray*) getConsumptionResultPerYear:(NSMutableArray*) dataArray
{
    if(dataArray == nil)
    {
        NSLog(@"Data Array is empty");
        return nil;
    }
    NSInteger count = [dataArray count];
    
    int prevYear = 0;
    double volume = 0;
    
    NSMutableArray *consumptionArray = [[NSMutableArray alloc] init];
    for( int i = 0; i < count; i++)
    {
        NSMutableDictionary *currentDictionary = [dataArray objectAtIndex:i];
        
        int currentYear = [self getYearFromQuarter:[currentDictionary objectForKey:@"quarter"]];

        if([self isSameYear:prevYear andCurrYear:currentYear])
        {
            double tempVolume = [[currentDictionary objectForKey:@"volume_of_mobile_data"] doubleValue];
            volume += tempVolume;
            
        }
        else {
            [consumptionArray addObject:[self getUserDataFromYear:prevYear andVol:volume] ];
            
            //resetting value of volume
            volume = [[currentDictionary objectForKey:@"volume_of_mobile_data"] doubleValue];
        }
        prevYear = currentYear;
        
    }
    
    [consumptionArray addObject:[self getUserDataFromYear:prevYear andVol:volume] ]; //get the last row as well
    return consumptionArray;
}
- (bool) isSameYear:(int)prevYear andCurrYear:(int)currYear
{
    return (prevYear == 0 || prevYear == currYear);
}
- (int) getYearFromQuarter:(NSString *) valueString
{
    NSRange range = [valueString rangeOfString:@"-"];
    NSString *newString = [valueString substringToIndex:range.location];
    int myInt = [newString intValue];
    //    NSLog(@"%d", myInt);
    return myInt;
}

- (UserData *)getUserDataFromYear:(int)year andVol:(double)volume
{
    NSString *yearStr = [NSString stringWithFormat:@"%d", year];
    NSString *volumeStr = [NSString stringWithFormat:@"%f", volume];
    
    UserData *data = [[UserData alloc] init];
    data.year = yearStr;
    data.volume = volumeStr;
    return data;
}
@end
