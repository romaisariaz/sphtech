//
//  ParseManager.m
//  MobileDataApp
//
//  Created by Romaisa Riaz on 18/08/2019.
//  Copyright © 2019 Romaisa Riaz. All rights reserved.
//

#import "ParseManager.h"


@implementation ParseManager
@synthesize parser;
@synthesize fields;
@synthesize result;

+ (id)sharedManager {
    static ParseManager *sharedMyManager = nil;
    @synchronized(self) {
        if (sharedMyManager == nil)
            sharedMyManager = [[self alloc] init];
    }
    return sharedMyManager;
}


- (id)init {
    if (self = [super init]) {
        parser = [[CSVParser alloc] initCSVParser:@"mobile-data-usage"];
        fields = [parser readField];
        result = [parser readResult];
    }
    return self;
}

- (void)dealloc {
    // Should never be called, but just here for clarity really.
}


@end
